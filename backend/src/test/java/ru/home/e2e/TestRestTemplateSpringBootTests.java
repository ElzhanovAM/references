package ru.home.e2e;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.jdbc.JdbcTestUtils;
import ru.home.domain.ReferenceInfo;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ActiveProfiles({"test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TestRestTemplateSpringBootTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Test
	@DirtiesContext
	void testRestGetAll() {
		List<ReferenceInfo> references = restTemplate.exchange("/api/references", HttpMethod.GET, null, new ParameterizedTypeReference<List<ReferenceInfo>>() {}).getBody();
		System.out.println(references);
		assertEquals(2, references.size());
	}

	@Test
	@DirtiesContext
	void testRestAdd() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<ReferenceInfo> entity = new HttpEntity<>(new ReferenceInfo(0, "Youtube", "https://youtube.com",
				new String[]{"video", "posts"}, false), headers);

		ResponseEntity<Object> response = restTemplate.exchange("/api/references", HttpMethod.POST, entity, Object.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());

		assertEquals(1, JdbcTestUtils.countRowsInTableWhere(jdbcTemplate, "references", "name = 'Youtube'"));
	}

	@Test
	@DirtiesContext
	void testRestAddExistReference() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<ReferenceInfo> entity = new HttpEntity<>(new ReferenceInfo(1, "Vkontakte", "https://vk.com",
				new String[]{"friends", "Vk"}, false), headers);

		ResponseEntity<Object> response = restTemplate.exchange("/api/references", HttpMethod.POST, entity, Object.class);

		assertEquals(HttpStatus.NOT_ACCEPTABLE, response.getStatusCode());
	}

	@Test
	@DirtiesContext
	void testRestAddInvalidReference() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<ReferenceInfo> entity = new HttpEntity<>(new ReferenceInfo(1, "Vkontakte", "vk.com",
				new String[]{"friends", "Vk"}, false), headers);

		ResponseEntity<Object> response = restTemplate.exchange("/api/references", HttpMethod.POST, entity, Object.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
}
