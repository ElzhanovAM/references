package ru.home.integration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.home.controller.ReferenceController;
import ru.home.domain.ReferenceInfo;
import ru.home.domain.ReferenceSearch;
import ru.home.service.ReferenceService;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {ReferenceController.class})
public class RestControllerMockMvcTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ReferenceService referenceService;


	@Test
	public void getAll() throws Exception {
		ReferenceInfo vkRef = new ReferenceInfo(1, "Vkontakte", "https://vk.com",
				new String[]{"friends", "Vk"}, false);

		ReferenceInfo youtubeRef = new ReferenceInfo(2, "Youtube", "https://youtube.com",
				new String[]{"video", "posts"}, true);

		when(referenceService.getAll()).thenReturn(List.of(vkRef, youtubeRef));

		mockMvc.perform(get("/api/references"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content()
						.json("[" +
								"{\"id\":1," +
								"\"name\":\"Vkontakte\"," +
								"\"value\":\"https://vk.com\"," +
								"\"tags\":[\"friends\", \"Vk\"]," +
								"\"isRead\":false}," +
								"{\"id\":2," +
								"\"name\":\"Youtube\"," +
								"\"value\":\"https://youtube.com\"," +
								"\"tags\":[\"video\", \"posts\"]," +
								"\"isRead\":true}" +
								"]"))
				.andExpect(jsonPath("$.length()").value(2));
	}

	@Test
	public void searchReference() throws Exception {
		ReferenceInfo vkRef = new ReferenceInfo(1, "Vkontakte", "https://vk.com",
				new String[]{"friends", "Vk"}, false);

		ReferenceSearch vkRefSearch = new ReferenceSearch("Vkontakte",
				"Vk", false);

		when(referenceService.getReferenceBySearch(vkRefSearch)).thenReturn(List.of(vkRef));

		mockMvc.perform(post("/api/references/search")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content("{" +
				"\"name\":\"Vkontakte\"," +
				"\"tag\":\"Vk\"," +
				"\"isRead\":false" +
				"}"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content()
						.json("[" +
								"{\"id\":1," +
								"\"name\":\"Vkontakte\"," +
								"\"value\":\"https://vk.com\"," +
								"\"tags\":[\"friends\", \"Vk\"]," +
								"\"isRead\":false}" +
								"]"))
				.andExpect(jsonPath("$.length()").value(1));
	}
}