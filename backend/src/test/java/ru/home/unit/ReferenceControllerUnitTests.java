package ru.home.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import ru.home.validator.ReferenceValidBeanConstraintValidator;


public class ReferenceControllerUnitTests {
    @Test
    void testAddReference(){
        ReferenceValidBeanConstraintValidator referenceValidBeanConstraintValidator = new ReferenceValidBeanConstraintValidator();
        Assertions.assertEquals(
                false,
                referenceValidBeanConstraintValidator.isValid("vk.com", null)
        );
    }
}