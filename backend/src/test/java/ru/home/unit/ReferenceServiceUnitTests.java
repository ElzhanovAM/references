package ru.home.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.home.domain.ReferenceInfo;
import ru.home.domain.ReferenceSearch;
import ru.home.exception.DuplicateReferenceException;
import ru.home.repository.H2ReferenceRepositoryImpl;
import ru.home.repository.ReferenceRepository;
import ru.home.service.ReferenceService;
import ru.home.service.ReferenceServiceImpl;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

public class ReferenceServiceUnitTests {

    @Test
    void testSearchReference(){
        // 1. Выбираем объект для Mock
        ReferenceRepository referenceRepository =
                Mockito.mock(H2ReferenceRepositoryImpl.class);

        ReferenceInfo vkRef = new ReferenceInfo(1, "Vkontakte", "https://vk.com",
                new String[]{"friends", "Vk"}, false);

        ReferenceInfo youtubeRef = new ReferenceInfo(2, "Youtube", "https://youtube.com",
                new String[]{"video", "posts"}, true);

        // 2. Мокируем вызов (т.к. возвращаемый результат не void, используем when + then)
        Mockito.when(referenceRepository.getAll())
                .thenReturn(
                        List.of(vkRef, youtubeRef)
                );

        ReferenceService referenceService = new ReferenceServiceImpl(
                referenceRepository
        );

        // 3. Проведение проверок
        Assertions.assertEquals(
                2,
                referenceService.getReferenceBySearch(new ReferenceSearch()).size()
        );

        Assertions.assertEquals(
                1,
                referenceService.getReferenceBySearch(new ReferenceSearch("Vkontakte",null,null)).size()
        );

        Assertions.assertEquals(
                1,
                referenceService.getReferenceBySearch(new ReferenceSearch(null,"video",null)).size()
        );

        Assertions.assertEquals(
                1,
                referenceService.getReferenceBySearch(new ReferenceSearch(null,null,true)).size()
        );

        Assertions.assertEquals(
                0,
                referenceService.getReferenceBySearch(new ReferenceSearch("FaceBook",null,true)).size()
        );

        Assertions.assertEquals(vkRef, referenceService.getReferenceBySearch(new ReferenceSearch("Vkontakte","Vk",false)).get(0));
        Mockito.verify(
                referenceRepository,
                Mockito.times(6)
        ).getAll();
        Mockito.verifyNoMoreInteractions(referenceRepository);
    }

    @Test
    void testAddReference(){
        // 1. Выбираем объект для Mock
        ReferenceRepository referenceRepository =
                Mockito.mock(H2ReferenceRepositoryImpl.class);

        ReferenceInfo vkRef = new ReferenceInfo(1, "Vkontakte1", "https://vk.com",
                new String[]{"friends", "Vk"}, false);

        ReferenceInfo addRef1 = new ReferenceInfo(2, "Vkontakte2", "https://vk.com",
                new String[]{"video", "posts"}, true);

        ReferenceInfo addRef2 = new ReferenceInfo(2, "Vkontakte1", "https://vk.ru",
                new String[]{"video", "posts"}, true);

        Mockito.when(referenceRepository.getAll())
                .thenReturn(
                        List.of(vkRef)
                );
        // т.к. возвращаемый результат void, используем do + when + метод
        Mockito.doNothing().when(
               referenceRepository
        ).add(any(ReferenceInfo.class)); // any - matcher

        ReferenceService referenceService = new ReferenceServiceImpl(
                referenceRepository
        );

        Assertions.assertThrows(
                DuplicateReferenceException.class,
                () -> referenceService.add(addRef1)
        );

        Assertions.assertThrows(
                DuplicateReferenceException.class,
                () -> referenceService.add(addRef2)
        );
    }
}