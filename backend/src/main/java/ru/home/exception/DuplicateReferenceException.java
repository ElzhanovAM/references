package ru.home.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class DuplicateReferenceException extends RuntimeException {
    public DuplicateReferenceException() {
        super();
    }

    public DuplicateReferenceException(String message) {
        super(message);
    }

    public DuplicateReferenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateReferenceException(Throwable cause) {
        super(cause);
    }

    protected DuplicateReferenceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
