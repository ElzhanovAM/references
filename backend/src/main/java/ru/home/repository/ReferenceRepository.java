package ru.home.repository;

import ru.home.domain.ReferenceInfo;

import java.util.List;

public interface ReferenceRepository {
    List<ReferenceInfo> getAll();

    void readRef(int id);

    void unreadRef(int id);

    void add(ReferenceInfo referenceInfo);

    void removeById(int id);
}
