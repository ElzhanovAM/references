package ru.home.repository;

import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.home.domain.ReferenceInfo;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.lang.String;
import java.util.Map;

@Repository
@Profile("test")
public class H2ReferenceRepositoryImpl implements ReferenceRepository {

    NamedParameterJdbcTemplate jdbcTemplate;

    public H2ReferenceRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<ReferenceInfo> getAll() {
        return jdbcTemplate.query("SELECT id, name, value, tags, read FROM references",
                new RowMapper<ReferenceInfo>() {
                    @Override
                    public ReferenceInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Array array = rs.getArray("tags");
                        Object[] tags =(Object[]) array.getArray();
                        String[] tagsS = Arrays.copyOf(tags, tags.length, String[].class);
                        return new ReferenceInfo(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("value"),
                                tagsS,
                                rs.getBoolean("read")
                        );
                    }
                });
    }

    @Override
    public void readRef(int id) {
        jdbcTemplate.update("UPDATE references SET read = TRUE where id = :id", Map.of("id", id));
    }

    @Override
    public void unreadRef(int id) {
        jdbcTemplate.update("UPDATE references SET read = FALSE where id = :id", Map.of("id", id));
    }

    @Override
    public void add(ReferenceInfo referenceInfo) {
        jdbcTemplate.update("INSERT INTO references (name, value, tags) VALUES\n" +
                "  (:name, :value, :tags)",
                Map.of("name", referenceInfo.getName(),
                        "value", referenceInfo.getValue(),
                        "tags", referenceInfo.getTags()));
    }

    @Override
    public void removeById(int id) {
        jdbcTemplate.update(
                "DELETE FROM references WHERE id = :id",
                Map.of("id", id)
        );
    }
}
