package ru.home.errorcontroller;


import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.home.response.RestErrorResponse;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/error")
public class CustomErrorController
    implements ErrorController {

  private final static String path = "/error";

  @Override
  public String getErrorPath() {
    return path;
  }

  // TODO: для не application/json оставить старые ошибки
  @RequestMapping
  public ResponseEntity<RestErrorResponse> error(HttpServletResponse httpServletResponse) {
    HttpStatus status = HttpStatus.valueOf(
        httpServletResponse.getStatus()
    );

    RestErrorResponse response = new RestErrorResponse(
        status.value(),
        "Something bad happened"
    );

    return new ResponseEntity<>(response, status);
  }
}
