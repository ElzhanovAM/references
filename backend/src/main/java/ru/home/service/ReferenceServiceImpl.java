package ru.home.service;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.home.domain.ReferenceInfo;
import ru.home.domain.ReferenceSearch;
import ru.home.exception.DuplicateReferenceException;
import ru.home.repository.ReferenceRepository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReferenceServiceImpl implements ReferenceService {

    private final ReferenceRepository referenceRepository;

    public ReferenceServiceImpl(ReferenceRepository referenceRepository) {
        this.referenceRepository = referenceRepository;
    }

    @Override
    public List<ReferenceInfo> getAll() {
        return referenceRepository.getAll();
    }

    @Override
    public void add(ReferenceInfo referenceInfo) {
        List<ReferenceInfo> presentReferences = referenceRepository.getAll();
        for (ReferenceInfo presentReference : presentReferences) {
            if(presentReference.getValue().equals(referenceInfo.getValue())) {
                throw new DuplicateReferenceException("Эта ссылка уже существует под именем " + presentReference.getName());
            }
            if(presentReference.getName().equals(referenceInfo.getName())) {
                throw new DuplicateReferenceException("Cсылка c именем " + referenceInfo.getName() + " уже существует");
            }
        }
        referenceRepository.add(referenceInfo);
    }

    @Override
    public void readRef(int id) {
        referenceRepository.readRef(id);
    }

    @Override
    public void unreadRef(int id) {
        referenceRepository.unreadRef(id);
    }

    @Override
    public void removeById(int id) {
        referenceRepository.removeById(id);
    }

    @Override
    public List<ReferenceInfo> getReferenceBySearch(ReferenceSearch referenceSearch) {
        List<ReferenceInfo> allReferences = referenceRepository.getAll();
        if(!StringUtils.isEmpty(referenceSearch.getName())) {
            allReferences = allReferences
                    .stream()
                    .filter(a -> a.getName().contains(referenceSearch.getName()))
                    .collect(Collectors.toList());
        }
        if(!StringUtils.isEmpty(referenceSearch.getTag())) {
            allReferences = allReferences
                    .stream()
                    .filter(a -> Arrays.asList(a.getTags()).contains(referenceSearch.getTag()))
                    .collect(Collectors.toList());
        }
        if(referenceSearch.getIsRead()!=null) {
            allReferences = allReferences
                    .stream()
                    .filter(a -> a.getIsRead().equals(referenceSearch.getIsRead()))
                    .collect(Collectors.toList());
        }
        return allReferences;
    }
}
