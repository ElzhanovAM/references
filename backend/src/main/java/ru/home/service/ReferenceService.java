package ru.home.service;

import ru.home.domain.ReferenceInfo;
import ru.home.domain.ReferenceSearch;

import java.util.List;

public interface ReferenceService {
    List<ReferenceInfo> getAll();

    void readRef(int id);

    void unreadRef(int id);

    void add(ReferenceInfo referenceInfo);

    void removeById(int id);

    List<ReferenceInfo> getReferenceBySearch(ReferenceSearch referenceSearch);
}
