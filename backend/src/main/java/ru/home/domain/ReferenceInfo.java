package ru.home.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.home.validator.ReferenceValueValidBeanConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReferenceInfo {
    int id;
    @NotNull(message="Название ссылки должно быть задано")
    @Size(min = 3, max = 20)
    String name;
    @NotNull(message="Значение ссылки должно быть задано")
    @ReferenceValueValidBeanConstraint(message = "Данная ссылка не может существовать")
    String value;
    String [] tags;
    Boolean isRead;
}
