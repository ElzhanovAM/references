package ru.home.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.home.domain.ReferenceInfo;
import ru.home.domain.ReferenceSearch;
import ru.home.exception.RestValidationException;
import ru.home.service.ReferenceService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/references")
public class ReferenceController {

    private final ReferenceService referenceService;

    public ReferenceController(ReferenceService referenceService) {
        this.referenceService = referenceService;
    }

    @GetMapping
    @ApiOperation("Get list of References")
    public List<ReferenceInfo> getAll() {
        return referenceService.getAll();
    }

    @PostMapping
    @ApiOperation("Add new Reference")
    public void add(@RequestBody @Valid ReferenceInfo referenceInfo, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new RestValidationException(bindingResult.getFieldError().getDefaultMessage());
        }
        referenceService.add(referenceInfo);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Remove Reference by id")
    public void delete(@PathVariable int id) {
        referenceService.removeById(id);
    }

    @PostMapping("/{id}/read")
    @ApiOperation("Read Reference")
    public void readRef(@PathVariable int id) {
        referenceService.readRef(id);
    }

    @PostMapping("/{id}/unread")
    @ApiOperation("Unread Reference")
    public void unreadRef(@PathVariable int id) {
        referenceService.unreadRef(id);
    }

    @PostMapping("/search")
    @ApiOperation("Search References")
    public List<ReferenceInfo> search(@RequestBody ReferenceSearch referenceSearch) {
        return referenceService.getReferenceBySearch(referenceSearch);
    }
}
