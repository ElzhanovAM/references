package ru.home.validator;

import org.apache.commons.validator.routines.UrlValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ReferenceValidBeanConstraintValidator implements ConstraintValidator<ReferenceValueValidBeanConstraint, String> {

    @Override
    public boolean isValid(String referenceValue, ConstraintValidatorContext constraintValidatorContext) {
        String[] schemes = {"http","https"};
        UrlValidator urlValidator = new UrlValidator(schemes);
        return urlValidator.isValid(referenceValue);
    }

    @Override
    public void initialize(ReferenceValueValidBeanConstraint constraintAnnotation) {

    }
}
