describe('App Spec', function() {
    it('should sum two values', function() {
        const result = sum(1, 2);
        expect(result).toBe(3);
    })
});