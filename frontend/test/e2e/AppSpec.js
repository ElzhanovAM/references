describe('todo list', function() {
    it('should add a todo', function() {
        browser.waitForAngularEnabled(false);
        browser.get('index.html');

        let references = element.all(by.css('.collection-item'));
        expect(references.count()).toBe(0);

        element(by.css('#name')).sendKeys('Youtube');
        element(by.css('#tags')).sendKeys('#video');
        element(by.css('#value')).sendKeys('https://youtube.com');
        element(by.css('#add')).click();

        browser.refresh();

        expect(references.count()).toBe(1);

        let last = element.all(by.css('#references')).last();

        let child = last.$('.material-icons');

        child.click();

        browser.refresh();

        expect(references.count()).toBe(0);

    });
});