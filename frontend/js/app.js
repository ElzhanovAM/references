const referencesElement = document.querySelector('#references');
const addElement = document.querySelector('#add');
const nameElement = document.querySelector('#name');
const valueElement = document.querySelector('#value');
const tagsElement = document.querySelector('#tags');
const labelElementForName = document.querySelector('label[for="name"]');
const labelElementForValue = document.querySelector('label[for="value"]');
const noReadElement= document.querySelector('#noread');
const readElement= document.querySelector('#read');
const addFormElement = document.querySelector('#addForm');
const searchElement= document.querySelector('#search');
const searchFormElement = document.querySelector('#searchForm');
const listElement = document.querySelector('#list');
const searchNameElement = document.querySelector('#searchName');
const searchTagElement = document.querySelector('#searchTag');
const searchButtonElement = document.querySelector('#searchButton');



getReferences(false);

function getReferences(isRead) {
    const request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:8080/api/references', true);
    request.addEventListener('readystatechange', function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
                referencesElement.innerText = '';
                const elements = JSON.parse(request.responseText);
                elements.forEach(function(el) {
                    if(el.isRead === isRead) {
                        addReferenceToList(el, false);
                    }
                });
            } else {
                console.log('Error');
            }
        }
    });
    request.send();
}

function addReference(reference) {
    const request = new XMLHttpRequest();
    request.open('POST', 'http://localhost:8080/api/references', true);
    request.addEventListener('readystatechange', function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            switch (request.status) {
                case 200 : {
                    getReferences(false);
                    break;
                }
                case 400 : {
                    const errorMsg = JSON.parse(request.responseText);
                    alert(errorMsg.message);
                    break;
                }
                case 406 : {
                    const errorMsg = JSON.parse(request.responseText);
                    alert(errorMsg.message);
                    break;
                }
                default : {
                    alert(request.statusText);
                }
            }
        }
    });
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify(
        {name: reference.name, value: reference.value, tags: reference.tags}
    ));
}

function searchReference(reference) {
    const request = new XMLHttpRequest();
    request.open('POST', 'http://localhost:8080/api/references/search', true);
    request.addEventListener('readystatechange', function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
                referencesElement.innerText = '';
                const elements = JSON.parse(request.responseText);
                elements.forEach(function(el) {
                    addReferenceToList(el, true);
                });
                listElement.setAttribute('style', 'display : inline');
            } else {
                console.log('Error');
            }
        }
    });
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify(
        {name : reference.name, tag : reference.tag, isRead : reference.read}
    ));
}

function removeReference(id, read) {
    const request = new XMLHttpRequest();
    request.open('DELETE', `http://localhost:8080/api/references/${id}`, true);
    request.addEventListener('readystatechange', function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
                if(read.toString() === 'true') {
                    getReferences(true);
                }
                if(read.toString() === 'false') {
                    getReferences(false);
                }
            } else {
                console.log('Error');
            }
        }
    });
    request.send();
}

function readReference(id, read) {
    const request = new XMLHttpRequest();
    if(read.toString() === 'true') {
        request.open('POST', `http://localhost:8080/api/references/${id}/unread`, true);
        request.addEventListener('readystatechange', function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    getReferences(true);
                } else {
                    console.log('Error');
                }
            }
        });
        request.send();
    }
    if(read.toString() === 'false') {
        request.open('POST', `http://localhost:8080/api/references/${id}/read`, true);
        request.addEventListener('readystatechange', function () {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    getReferences(false);
                } else {
                    console.log('Error');
                }
            }
        });
        request.send();
    }
}

function addReferenceToList(el, bySearch) {
    const li = document.createElement('li');
    li.classList.add('collection-item');
    li.setAttribute('data-id', el.id);
    if(bySearch) {
        li.innerHTML = `
        <input id="${el.id}" class="inputCheck" name="${el.isRead}" type="checkbox" disabled/>  
        <label for="${el.id}">
            <a href="${el.value}"> ${el.name}</a>
        </label>
        `;
    } else {
        li.innerHTML = `
        <input id="${el.id}" class="inputCheck" name="${el.isRead}" type="checkbox" enabled/>  
        <label for="${el.id}">
            <a href="${el.value}"> ${el.name}</a>
        </label>
        <a href="#" class="remove-reference secondary-content"><i class="material-icons right">delete</i></a>
        `;
    }
    const inputCheck = li.querySelector('.inputCheck');
    inputCheck.setAttribute('isRead', el.isRead);
    if (el.isRead) inputCheck.setAttribute('checked', 'checked');
    const tags = el.tags;
    tags.forEach(function(tag) {
        const a = document.createElement('a');
        a.setAttribute('href', '#');
        a.innerHTML = `
        <span id = "${tag}" class="badge">#${tag}</span>
        `;
        li.appendChild(a);
    });
    referencesElement.appendChild(li);
}

addElement.addEventListener('click', function (e) {
    e.preventDefault();
    const name =  nameElement.value.trim();
    const value = valueElement.value.trim();
    const tags = tagsElement.value.trim();
    if (name === '' ) {
        nameElement.classList.add('invalid');
        labelElementForName.setAttribute(
            'data-error',
            'Name reference can\'t be empty'
        );
        valueElement.value = '';
        tagsElement.value = '';
    } else if(value === ''){
        valueElement.classList.add('invalid');
        labelElementForValue.setAttribute(
            'data-error',
            'Value reference can\'t be empty'
        );
        nameElement.value = '';
        tagsElement.value = '';
    } else {
        const reference = new Reference(name, value, tags);
        addReference(reference);
        nameElement.value = '';
        valueElement.value = '';
        tagsElement.value = '';
    }
});

searchButtonElement.addEventListener('click', function (e) {
    e.preventDefault();
    const name = searchNameElement.value.trim();
    const tag = searchTagElement.value.trim();
    let read;
    if (document.getElementById('readRef').checked) {
        read = document.getElementById('readRef').value;
    } else if (document.getElementById('unReadRef').checked){
        read = document.getElementById('unReadRef').value;
    }
    const referenceSearch = new ReferenceSearch(name, tag, read);
    searchReference(referenceSearch);
});

referencesElement.addEventListener('click', function (e) {
    if (e.target.parentElement.classList.contains('remove-reference')) {
        const li = e.target.parentElement.parentElement;
        const inputCheck = li.querySelector('.inputCheck');
        const read = inputCheck.getAttribute('isRead');
        const id = li.getAttribute('data-id');
        removeReference(id, read);
    } else if(e.target.id === e.target.parentElement.getAttribute('data-id')) {
        const id = e.target.id;
        let read = e.target.getAttribute('isRead');
        readReference(id, read);
    } else if (e.target.classList.contains('badge')){
        e.preventDefault();
        addFormElement.setAttribute('style', 'display : none');
        listElement.setAttribute('style', 'display : none');
        searchFormElement.setAttribute('style', 'display : inline');
        readElement.parentElement.setAttribute('class', '');
        noReadElement.parentElement.setAttribute('class', '');
        searchElement.parentElement.setAttribute('class', 'active');
        const tag = e.target.id;
        console.log(tag);
        const referenceSearch = new ReferenceSearch(name, tag);
        searchReference(referenceSearch);
        searchTagElement.value = tag;
    }
});

noReadElement.addEventListener('click', function (e) {
    searchFormElement.setAttribute('style', 'display : none');
    addFormElement.setAttribute('style', 'display : inline');
    listElement.setAttribute('style', 'display : inline');
    readElement.parentElement.setAttribute('class', '');
    searchElement.parentElement.setAttribute('class', '');
    noReadElement.parentElement.setAttribute('class', 'active');
    searchNameElement.value = '';
    searchTagElement.value = '';

    getReferences(false);
});


readElement.addEventListener('click', function (e) {
    searchFormElement.setAttribute('style', 'display : none');
    addFormElement.setAttribute('style', 'display : none');
    listElement.setAttribute('style', 'display : inline');
    noReadElement.parentElement.setAttribute('class', '');
    searchElement.parentElement.setAttribute('class', '');
    readElement.parentElement.setAttribute('class', 'active');
    searchNameElement.value = '';
    searchTagElement.value = '';
    getReferences(true);
});

searchElement.addEventListener('click', function (e) {
    addFormElement.setAttribute('style', 'display : none');
    searchFormElement.setAttribute('style', 'display : inline');
    listElement.setAttribute('style', 'display : none');
    readElement.parentElement.setAttribute('class', '');
    noReadElement.parentElement.setAttribute('class', '');
    searchElement.parentElement.setAttribute('class', 'active');
    nameElement.value='';
    tagsElement.value='';
});

class Reference {
    constructor(name, value, tags) {
        this.name = name;
        this.value = value;
        this.tags = tags.substr(1, tags.length).split(' #');
    }
}

class ReferenceSearch {
    constructor(name, tag, read) {
        this.name = name;
        this.tag = tag;
        this.read = read;
    }
}